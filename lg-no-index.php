<?php
/**
* Plugin Name: Longevity Graphics No Index
* Plugin URI: https://www.longevitygraphics.com/
* Author: Stefan Adam, Longevity Graphics
* Description: This is a simple noindexing plugin designed to keep staging and local websites from accidentally being indexed.
* Version: 1.1.3
*/

add_action('send_headers', 'add_noindex_header');

function add_noindex_header() {
	header('X-Robots-Tag: noindex');
}


add_action('wp_head', 'add_noindex_meta'); 

function add_noindex_meta() {
	?>
		<meta name="robots" content="noindex" />
	<?php
}

function active_admin_notice(){
     echo '<div class="notice notice-error">
             <p>The Longevity Graphics No Index Plugin is Active. If this is not a local or staging site, this should be disabled immediately!</p>
         </div>';
    
}
add_action('admin_notices', 'active_admin_notice');

function deactivate_on_live(){ 
	$should_deactivate = true;
	if ( strpos($_SERVER['HTTP_HOST'], 'longevitystaging.com') !== true) { 
		$should_deactivate = false;	
	}
	if ( strpos($_SERVER['HTTP_HOST'], '.test') !== true) { 
		$should_deactivate = false;	
	}

	if ($should_deactivate === true) {
		deactivate_plugins( plugin_basename(__FILE__), true);
	}
	
}
add_action( 'admin_init', 'deactivate_on_live')



?>